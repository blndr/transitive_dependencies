# Transitive dependencies

The purpose of this program is to expand the given dependencies by following their transitivity.
It takes a file in input with a strict structure (see `examples/transitive.deps`) and outputs the expanded dependencies using the same format.
It uses a DFS (a.k.a. Depth First Search) algorithm to travel through the dependency graph.
The original kata can be found here => http://codekata.com/kata/kata18-transitive-dependencies/

## Execution
```
. venv/bin/activate
python runner.py
```

## Requirements
Python 3.x is required.
Please setup your virtualenv with Python 3 and install the requirements using the ```requirements.txt``` file provided.
```
. venv/bin/activate
pip install -r requirements.txt
```

## Unit test execution
```
. venv/bin/activate
pytest
```
