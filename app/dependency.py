from app.node import Node


def expand(modules):
    graph = to_graph(modules.split('\n'))
    simplification = ''

    for node in graph:
        simplification += pretty_print(travel(graph, node))
    return simplification


def to_graph(modules):
    graph = []
    for literal_node in modules:
        split = literal_node.replace(' ', '').split('>')
        graph.append(Node(split[0], [Node(neighbor) for neighbor in split[1]]))

    return graph


def travel(graph, starting_node, visited=None):
    if not visited:
        visited = []

    current_node = next(matching_node(graph, starting_node), starting_node)

    if current_node not in visited:
        visited.append(current_node)
        for neighbor in current_node.neighbors:
            travel(graph, neighbor, visited)

    return visited


def pretty_print(visited):
    pretty = '%s >' % str(visited[0])
    for dependency in sorted(visited[1:]):
        pretty += ' %s' % str(dependency)
    return str(pretty) + '\n'


def matching_node(graph, starting_node):
    return (node for node in graph if node == starting_node)
