class Node:
    def __init__(self, name, neighbors=[]):
        self.name = name
        self.neighbors = neighbors

    def __eq__(self, other):
        return other.name == self.name

    def __repr__(self):
        return self.name

    def __hash__(self, *args, **kwargs):
        return super().__hash__(*args, **kwargs)

    def __lt__(self, other):
        return self.name < other.name

    def __gt__(self, other):
        return self.name > other.name