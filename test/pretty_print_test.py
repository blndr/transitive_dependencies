import unittest

from app.dependency import pretty_print
from app.node import Node


class PrettyPrintTest(unittest.TestCase):
    def test_returns_a_single_node_name_if_one_visited_node(self):
        # given
        visited = [Node('A')]

        # when
        print = pretty_print(visited)

        # then
        assert print == 'A >\n'

    def test_returns_all_node_names_with_the_first_as_starting_node(self):
        # given
        visited = [Node('A'), Node('B'), Node('C'), Node('D')]

        # when
        print = pretty_print(visited)

        # then
        assert print == 'A > B C D\n'
