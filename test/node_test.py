import unittest

from app.node import Node


class NodeTest(unittest.TestCase):
    def test_a_new_node_has_a_name_but_no_neighbors_by_default(self):
        assert Node('A').name == 'A'
        assert Node('A').neighbors == []
