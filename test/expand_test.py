import unittest

from app.dependency import expand


class ExpandTest(unittest.TestCase):
    def test_returns_the_full_dependencies_for_each_module(self):
        # given
        modules = 'A > B C\n' \
                  'B > C E\n' \
                  'C > G\n' \
                  'D > A F\n' \
                  'E > F\n' \
                  'F > H'

        # when
        simplified = expand(modules)

        # then
        assert simplified == 'A > B C E F G H\n' \
                             'B > C E F G H\n' \
                             'C > G\n' \
                             'D > A B C E F G H\n' \
                             'E > F H\n' \
                             'F > H\n'
