import unittest

from app.dependency import to_graph
from app.node import Node


class ToGraphTest(unittest.TestCase):
    def test_to_graph_returns_a_graph_representation_of_two_single_transitive_dependencies(self):
        # given
        modules = ['A > B', 'B > C']

        # when
        graph = to_graph(modules)

        # then
        assert len(graph) == 2
        assert graph[0] == Node('A')
        assert graph[0].neighbors == [Node('B')]
        assert graph[1] == Node('B')
        assert graph[1].neighbors == [Node('C')]

    def test_to_graph_returns_a_graph_representation_of_two_double_isolated_dependencies(self):
        # given
        modules = ['A > B C', 'D > E F']

        # when
        graph = to_graph(modules)

        # then
        assert len(graph) == 2
        assert graph[0].name == 'A'
        assert graph[0].neighbors == [Node('B'), Node('C')]
        assert graph[1].name == 'D'
        assert graph[1].neighbors == [Node('E'), Node('F')]
