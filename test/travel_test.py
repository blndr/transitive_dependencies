import unittest

from app.dependency import travel
from app.node import Node


class TravelTest(unittest.TestCase):
    def test_returns_one_node_if_graph_has_only_one_node(self):
        # given
        graph = [Node('A')]

        # when
        visited = travel(graph, Node('A'))

        # then
        assert visited == [Node('A')]

    def test_returns_two_nodes_when_one_node_with_one_neighbor(self):
        # given
        graph = [Node('A', [Node('B')])]

        # when
        visited = travel(graph, Node('A'))

        # then
        assert visited == [Node('A'), Node('B')]

    def test_returns_three_nodes_when_one_node_with_two_neighbors(self):
        # given
        graph = [Node('A', [Node('B'), Node('C')])]

        # when
        visited = travel(graph, Node('A'))

        # then
        assert visited == [Node('A'), Node('B'), Node('C')]

    def test_returns_three_nodes_when_two_nodes_connected_through_one_neighbor(self):
        # given
        nodeA = Node('A', [Node('B')])
        nodeB = Node('B', [Node('C')])
        graph = [nodeA, nodeB]

        # when
        visited = travel(graph, Node('A'))

        # then
        assert visited == [Node('A'), Node('B'), Node('C')]

    def test_returns_two_nodes_when_two_nodes_connected_through_one_neighbor_starting_with_it(self):
        # given
        nodeA = Node('A', [Node('B')])
        nodeB = Node('B', [Node('C')])
        graph = [nodeA, nodeB]

        # when
        visited = travel(graph, Node('B'))

        # then
        assert visited == [Node('B'), Node('C')]
