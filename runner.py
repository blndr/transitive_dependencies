# !/usr/bin/env python
from app.dependency import expand

with open('examples/transitive.deps') as f:
    content = f.readlines()

joined_content = ''.join(content)
print('\nBulk dependency graph :')
print(joined_content)
print('\nSimplified dependency graph :')
print(expand(joined_content))
